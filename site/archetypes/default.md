---
title: "{{ .Name | humanize | title }}"
date: {{ .Date }}
draft: true
hidden: true
summary: ""
image: "{{ getenv "PUBLIC_URL_PREFIX" }}/{{ .Name }}/tn_original.jpg"
openseadragon:
    url: "{{ getenv "PUBLIC_URL_PREFIX" }}/{{ .Name }}/image.dzi"
---

