---
title: "Tarantula Nebula"
date: 2021-07-28T14:07:06+10:00
draft: false
image: https://b2-anu.appf.org.au/file/acacia/zoomable-images/30_Doradus/tn_original.jpg
summary: The Tarantula Nebula (30 Doradus)
openseadragon:
    url: "https://b2-anu.appf.org.au/file/acacia/zoomable-images/30_Doradus/image.dzi"
---

The Tarantula Nebula (also known as 30 Doradus) is an H II region in the Large Magellanic Cloud (LMC), from the Solar System's perspective forming its south-east corner. 

This image was released to celebrate the 22nd anniversary of Hubble's launch. 


Source: [NASA APOD](https://apod.nasa.gov/apod/ap120516.html)