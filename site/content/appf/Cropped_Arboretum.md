---
title: "Arboretum (Cropped)"
date: 2021-07-28T16:49:05+10:00
draft: false
image: https://b2-anu.appf.org.au/file/acacia/zoomable-images/Cropped_Arboretum/tn_original.jpg
summary: "Cropped section from the National Arboretum Gigavision camera"
openseadragon:
    url: "https://b2-anu.appf.org.au/file/acacia/zoomable-images/Cropped_Arboretum/image.dzi"
---

