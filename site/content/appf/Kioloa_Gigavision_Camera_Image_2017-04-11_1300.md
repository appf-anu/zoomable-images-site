---
title: "Kioloa Gigavision Camera 2017-04-11"
date: 2021-07-28T16:15:08+10:00
draft: false
summary: "Midday Image from Kioloa on 2017-04-11"
image: https://b2-anu.appf.org.au/file/acacia/zoomable-images/Kioloa_Gigavision_Camera_Image_2017-04-11_1300/tn_original.jpg
openseadragon:
    url: "https://b2-anu.appf.org.au/file/acacia/zoomable-images/Kioloa_Gigavision_Camera_Image_2017-04-11_1300/image.dzi"
---

Midday Image from Kioloa on 2017-04-11